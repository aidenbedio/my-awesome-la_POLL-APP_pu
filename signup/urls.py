# from django.conf.urls import url
# from mysite.signup import views as signup_views

# urlpatterns = [
# 	url(r'^signup/$', signup_views.signup, name='signup'),
# ] 


from django.urls import path

from . import views

app_name = 'signup'
urlpatterns = [
	path('', views.signup, name = 'signup'),
	# path('<int:pk>/', views.DetailView.as_view(), name = 'detail'),
	# path('<int:pk>/results/', views.ResultsView.as_view(), name = 'results'),
	# path('<int:question_id>/vote/', views.vote, name = 'vote'),
]