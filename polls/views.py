from django.utils import timezone

from django.shortcuts import render, get_object_or_404, redirect
# from django.http import Http404
# from django.template import loader
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.views import generic

from .models import Question, Choice
# Create your views here.

# def index(request):
# 	latest_question_list = Question.objects.order_by('-pub_date')[:5]
# 	# output = ', '.join([q.question_text for q in latest_question_list])
# 	# return HttpResponse(output)
# 	context = {
# 		'latest_question_list': latest_question_list
# 	}
# 	return render(request, 'polls/index.html', context)

class IndexView(LoginRequiredMixin,generic.ListView):
	login_url = '/home'
	redirect_field_name = 'redirect_to'

	template_name = 'polls/index.html'
	context_object_name = 'latest_question_list'

	def get_queryset(self):
		return Question.objects.order_by('-pub_date')[:5]


# def detail (request, question_id):
# 	question = get_object_or_404(Question, pk=question_id)
# 	return render(request, 'polls/detail.html', {'question':question})
# 	# try:
# 	# 	question = Question.objects.get(pk=question_id)
# 	# except Question.DoesNotExist:
# 	# 	raise Http404("Question does not exist")
# 	# return render(request, 'polls/detail.html', {'question': question})

class DetailView(LoginRequiredMixin, generic.DetailView):
	login_url = '/home'
	redirect_field_name = 'redirect_to'

	model = Question
	template_name = 'polls/detail.html'

	def get_queryset(self):
		return Question.objects.filter(pub_date__lte=timezone.now())

# def results(request, question_id):
# 	# response = "You're looking at the results of question %s."
# 	# return HttpResponse(response % question_id)
# 	question = get_object_or_404(Question,pk=question_id)
# 	return render(request, 'polls/results.html', {'question':question})

class ResultsView(LoginRequiredMixin,generic.DetailView):
	login_url = '/home'
	redirect_field_name = 'redirect_to'

	model = Question
	template_name = 'polls/results.html'

def vote(request, question_id):
	# return HttpResponse("You're voting on question %s" % question_id)

	if request.user.is_authenticated:

		question = get_object_or_404(Question, pk = question_id)
		try:
			selected_choice = question.choice_set.get(pk=request.POST['choice'])
		except (KeyError, Choice.DoesNotExist):
			return render(request, 'polls/detail.html',{
				'question': question,
				'error_message': "You didn't select a choice",
				})
		else:
			selected_choice.votes += 1
			selected_choice.save()

			return HttpResponseRedirect(reverse('polls:results',args = (question.id,)))
	else:
		return redirect('/home')


def get_queryset(self):
	return Question.objects.filter(
		pub_date__lte=timezone.now()
	).order_by('-pub_date')[:5]
